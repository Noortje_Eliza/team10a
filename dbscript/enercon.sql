-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Gegenereerd op: 06 jan 2021 om 16:31
-- Serverversie: 10.1.13-MariaDB
-- PHP-versie: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `enercon`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `akkoord`
--

CREATE TABLE `akkoord` (
  `akkoord_id` int(11) NOT NULL,
  `akkoord` tinyint(1) DEFAULT NULL,
  `gebruiker_id` int(11) NOT NULL,
  `schakelbrief_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `gebruiker`
--

CREATE TABLE `gebruiker` (
  `gebruiker_id` int(11) NOT NULL,
  `emailadres` varchar(255) NOT NULL,
  `voornaam` varchar(100) NOT NULL,
  `achternaam` varchar(100) NOT NULL,
  `telefoonnummer` varchar(100) NOT NULL,
  `wachtwoord` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Gegevens worden geëxporteerd voor tabel `gebruiker`
--

INSERT INTO `gebruiker` (`gebruiker_id`, `emailadres`, `voornaam`, `achternaam`, `telefoonnummer`, `wachtwoord`) VALUES
(1, 'admin@enercon.com', 'admin', 'istrator', '12341234', '$2y$10$S2gutgYn37lweFpDMynHeezj/xkKD//qjAqkm7EGGHNLA4wAnIJ4.'),
(2, 'janpietje@gmail.com', 'jan', 'pietje', '12331233', '$2y$10$S2gutgYn37lweFpDMynHeezj/xkKD//qjAqkm7EGGHNLA4wAnIJ4.'),
(3, 'klaasjan@hotmail.com', 'klaas', 'jan', '12231223', '$2y$10$S2gutgYn37lweFpDMynHeezj/xkKD//qjAqkm7EGGHNLA4wAnIJ4.'),
(4, 'hansjoker@gmail.com', 'hans', 'joker', '12351235', '$2y$10$S2gutgYn37lweFpDMynHeezj/xkKD//qjAqkm7EGGHNLA4wAnIJ4.');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `gebruikerrolregel`
--

CREATE TABLE `gebruikerrolregel` (
  `gebruikerrolregel_id` int(11) NOT NULL,
  `gebruikerid` int(11) NOT NULL,
  `rolid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Gegevens worden geëxporteerd voor tabel `gebruikerrolregel`
--

INSERT INTO `gebruikerrolregel` (`gebruikerrolregel_id`, `gebruikerid`, `rolid`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 3, 3),
(4, 4, 4);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `rol`
--

CREATE TABLE `rol` (
  `rolid` int(11) NOT NULL,
  `code` varchar(2) NOT NULL,
  `omschrijving` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Gegevens worden geëxporteerd voor tabel `rol`
--

INSERT INTO `rol` (`rolid`, `code`, `omschrijving`) VALUES
(1, 'AD', 'Administrator'),
(2, 'IV', 'Installatieverantwoordelijke'),
(3, 'WV', 'Werkverantwoordelijke'),
(4, 'PL', 'Ploegleider');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `schakelbrief`
--

CREATE TABLE `schakelbrief` (
  `schakelbrief_id` int(11) NOT NULL,
  `datum_werkzaamheden` int(11) NOT NULL,
  `reden` int(11) NOT NULL,
  `opmerking_go_nl` varchar(255) NOT NULL,
  `opmerking_redenwerkzaamheden` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `schakelbrief_werkzaamheden`
--

CREATE TABLE `schakelbrief_werkzaamheden` (
  `schakelbrief_werkzaamheden_id` int(11) NOT NULL,
  `schakelbrief_id` int(11) NOT NULL,
  `werkzaamheden_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `werkzaamheden`
--

CREATE TABLE `werkzaamheden` (
  `werkzaamheden_id` int(11) NOT NULL,
  `plaats` varchar(100) NOT NULL,
  `veld` varchar(100) NOT NULL,
  `omschrijving_taak` varchar(100) NOT NULL,
  `uitgevoerd` tinyint(1) DEFAULT NULL,
  `uitgevoerd_dat_tijd` datetime NOT NULL,
  `turbine_serienummer` int(11) NOT NULL,
  `windpark` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexen voor geëxporteerde tabellen
--

--
-- Indexen voor tabel `akkoord`
--
ALTER TABLE `akkoord`
  ADD PRIMARY KEY (`akkoord_id`);

--
-- Indexen voor tabel `gebruiker`
--
ALTER TABLE `gebruiker`
  ADD PRIMARY KEY (`gebruiker_id`);

--
-- Indexen voor tabel `gebruikerrolregel`
--
ALTER TABLE `gebruikerrolregel`
  ADD PRIMARY KEY (`gebruikerrolregel_id`);

--
-- Indexen voor tabel `rol`
--
ALTER TABLE `rol`
  ADD PRIMARY KEY (`rolid`);

--
-- Indexen voor tabel `schakelbrief_werkzaamheden`
--
ALTER TABLE `schakelbrief_werkzaamheden`
  ADD PRIMARY KEY (`schakelbrief_werkzaamheden_id`);

--
-- Indexen voor tabel `werkzaamheden`
--
ALTER TABLE `werkzaamheden`
  ADD PRIMARY KEY (`werkzaamheden_id`);

--
-- AUTO_INCREMENT voor geëxporteerde tabellen
--

--
-- AUTO_INCREMENT voor een tabel `akkoord`
--
ALTER TABLE `akkoord`
  MODIFY `akkoord_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT voor een tabel `gebruiker`
--
ALTER TABLE `gebruiker`
  MODIFY `gebruiker_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT voor een tabel `gebruikerrolregel`
--
ALTER TABLE `gebruikerrolregel`
  MODIFY `gebruikerrolregel_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT voor een tabel `rol`
--
ALTER TABLE `rol`
  MODIFY `rolid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT voor een tabel `schakelbrief_werkzaamheden`
--
ALTER TABLE `schakelbrief_werkzaamheden`
  MODIFY `schakelbrief_werkzaamheden_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT voor een tabel `werkzaamheden`
--
ALTER TABLE `werkzaamheden`
  MODIFY `werkzaamheden_id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
