<?php
require_once "config/database.php";
require_once "templates/header.php";
?>

<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.bundle.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<body>



<!-- schakelbrief -->
<div class="container-fluid">
    <!-- header -->
    <div class="d-flex justify-content-center">
        <h1 class="display-1">Schakelbrief</h1>
    </div>
    <!-- einde header -->
    <!-- Open alignment div voor schakelbrief -->
    <div class="d-flex justify-content-center">
        <!--open schakelbrief container div -->
        <div class="col-10">
            <!-- open een formulier voor de schakelbrief -->
            <form method = "POST" action = "<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
                <!-- informatie schakelbrief -->
                <div class="row">
                    <div class="col-6 border">
                        <!-- form group schakelbrief nummer -->
                        <div class="form-group row mt-3">
                            <label for="schakelbriefnummer"
                                   class="col-xl-5 col-form-label col-form-label-lg">Schakelbriefnummer:</label>
                            <div class="col-xl">
                                <input type="text" class="form-control form-control-lg"
                                       id="schakelbriefnummer" placeholder="1234213">
                            </div>
                        </div>
                        <!-- form group windpark naam -->
                        <div class="form-group row row mt-3 border-top">
                            <label for="windpark" class="col-xl-5 col-form-label col-form-label-lg mt-3">Windpark
                                naam:</label>
                            <div class="col-xl mt-3">
                                <input type="text" class="form-control form-control-lg" id="windpark"
                                       placeholder="Kies hier een windpark">
                            </div>
                        </div>
                        <!-- form group datum werkzaamheden naam -->
                        <div class="form-group row row mt-3 row mt-3 border-top">
                            <label for="datwerkzaamheden"
                                   class="col-xl-5 col-form-label col-form-label-lg mt-3">Datum
                                werkzaamheden:</label>
                            <div class="col-xl mt-3">
                                <input type="datetime-local" class="form-control form-control-lg"
                                       id="datwerkzaamheden" placeholder="Datum">
                            </div>
                        </div>
                    </div>
                </div>

                <!-- informatie personen -->
                <div class="row mt-5" id="collatie_personen">
                    <!-- colum IV persoon -->
                    <div class="col-4 border">
                        <div class="row mt-3" id="personen_naam">
                            <label for="naam_iv" class="col-xl-3 col-form-label">Naam IV:</label>
                            <div class="col-xl">
                                <input type="text" class="form-control" id="naam_iv"
                                       placeholder="Installatieverantwoordelijke">
                            </div>
                        </div>
                        <div class="row mb-3 mt-3" id="personen_telnummer">
                            <label for="ivtelefoonnummer" class="col-xl-3 col-form-label">Tel.Nr.:</label>
                            <div class="col">
                                <input type="tel" class="form-control" id="tel_iv"
                                       placeholder="06-12341234">
                            </div>
                        </div>
                    </div>
                    <!-- colum WV persoon -->
                    <div class="col-4 border">
                        <div class="row mt-3">
                            <label for="naam_wv" class="col-xl-3 col-form-label">Naam WV:</label>
                            <div class="col-xl">
                                <input type="text" class="form-control" id="naam_wv"
                                       placeholder="Kies werkverantwoordelijke">
                            </div>
                        </div>
                        <div class="row mt-3 mb-3">
                            <label for="wvtelefoonnummer" class="col-xl-3 col-form-label">Tel.Nr.:</label>
                            <div class="col">
                                <input type="tel" class="form-control" id="tel_wv"
                                       placeholder="06-12341234">
                            </div>
                        </div>
                    </div>
                    <!-- colum PL persoon -->
                    <div class="col-4 border">
                        <div class="row mt-3" id="personen_naam">
                            <label for="naam_pl" class="col-xl-3 col-form-label">Naam PL:</label>
                            <div class="col-xl">
                                <input type="text" class="form-control" id="naam_pl"
                                       placeholder="Ploegleider">
                            </div>
                        </div>
                        <div class="row mb-3 mt-3" id="personen_telnummer">
                            <label for="pltelefoonnummer" class="col-xl-3 col-form-label">Tel.Nr.:</label>
                            <div class="col">
                                <input type="tel" class="form-control" id="tel_pl"
                                       placeholder="06-12341234">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Row info schakelbedrijd en contactpersoon -->
                <div class="row mt-5">
                    <!-- info schakelbedrijf -->
                    <div class="col-4 border">
                        <div class="row mt-3">
                            <label for="schakelbedrijf"
                                   class="col-xl-3 col-form-label">Schakelbedrijf:</label>
                            <div class="col-xl">
                                <input type="text" class="form-control" id="schakelbedrijf"
                                       placeholder="Schakelbedrijf">
                            </div>
                        </div>
                        <div class="row mb-3 mt-3" id="personen_telnummer">
                            <label for="schakelbedrijdtelefoonnummer"
                                   class="col-xl-3 col-form-label">Tel.Nr.:</label>
                            <div class="col">
                                <input type="tel" class="form-control" id="schakelbedrijdtelefoonnummer"
                                       placeholder="06-12341234">
                            </div>
                        </div>
                    </div>
                    <!-- info contact persoon -->
                    <div class="col-5 border">
                        <div class="row mt-3">
                            <label for="contactpersoon" class="col-xl-3 col-form-label">Contact
                                persoon:</label>
                            <div class="col-xl">
                                <input type="text" class="form-control" id="contactpersoon"
                                       placeholder="Contact persoon">
                            </div>
                        </div>
                        <div class="row mb-3 mt-3" id="personen_telnummer">
                            <label for="schakelbedrijdtelefoonnummer"
                                   class="col-xl-3 col-form-label">Tel.Nr.:</label>
                            <div class="col">
                                <input type="tel" class="form-control" id="schakelbedrijdtelefoonnummer"
                                       placeholder="06-12341234">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- info reden van schakelbrief -->
                <div class="row mt-5">
                    <div class="col">
                        <div class="form-group">
                            <label for="reden">Reden voor schakeling:</label>
                            <textarea class="form-control" rows="5" id="reden"></textarea>
                        </div>
                    </div>
                </div>
                <!-- info Opmerking Go-Nl -->
                <div class="row mt-2">
                    <div class="col">
                        <div class="form-group">
                            <label for="opmerking_go_nl">Opmerkingen Go-NL:</label>
                            <textarea class="form-control" rows="5" id="opmerking_go_nl"></textarea>
                        </div>
                    </div>
                </div>

                <!-- -->
                <div class="row mt-2">
                    <table class="table text-center" id="Werkzaamheden" border="1">
                        <thead>
                        <tr>
                            <th scope="col">Stap</th>
                            <th scope="col">Turbine</th>
                            <th scope="col">plaats</th>
                            <th scope="col">veld</th>
                            <th scope="col">omschrijving</th>
<!--                            <th scope="col"></th>-->
                            <th scope="col">Datum/Tijd</th>
                        </tr>
                        </thead>
                        <tr>
                            <td>1</td>
                            <td><select id="windturbine[]">
                                    <option value="volvo">x12321</option>
                                    <option value="saab">x12323</option>
                                    <option value="fiat">x12327</option>
                                    <option value="audi">x1232</option>
                                </select></td>
                            <td>
                                <select id="plaat[]">
                                    <option value="volvo">=164-1 (Remote1)</option>
                                    <option value="saab">=164-2 (Remote2)</option>
                                    <option value="fiat">=164-3 (Remote3)</option>
                                    <option value="audi">=005 (best.last)</option>
                                </select>

                            </td>
                            <td><select id="veld[]">
                                    <option value="volvo">Alle velden</option>
                                    <option value="saab">1</option>
                                    <option value="fiat">2</option>
                                    <option value="audi">3</option>
                                </select></td>
                            <td><input size=25 type="text" id="omschrijving[]"></td>
<!--                            <td><input size="2" type="checkbox" id="checked[]"></td>-->
                            <td><input size=25 type="datetime-local" id="lngbox[]"></td>
                        </tr>
                    </table>
                </div>
                <table>
                    <tr>
                        <!--<td><input class="btn btn-primary" type="button" id="verwijder" value="Delete"
                                   onclick="" disabled/></td>-->
                        <td><input class="btn btn-primary" type="button" id="btnAdd" value="Stap toevoegen"
                                   onclick=""/></td>
                    </tr>
                </table>
                <div class="row mt-4">
                    <input type="submit" class="btn btn-primary" name="versturen" value="versturen">
                </div>
        </div>
        </form>
        <!-- sluiten schakelbrief container div -->
    </div>
    <!-- sluiten schakelbrief alignment div -->
</div>
<!-- Sluiten container div -->
</div>
<script>

    $("#btnAdd").on("click", function () {
        var $tableBody = $('#Werkzaamheden').find("tbody"),
            $trLast = $tableBody.find("tr:last"),
            $trNew = $trLast.clone();


        $trLast.after($trNew);
    });

    console.log(<?php echo json_encode($_POST); ?>);
</script>
</body>
</html>
