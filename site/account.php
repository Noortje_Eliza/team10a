<!DOCTYPE html>
<html lang="en">
<?php

require_once "domein/gebruikercontroller.php";
// start sessie indien nodig
if(!isset($_SESSION)) 
{ 
	session_start(); 
} 
	
if (isset($_POST['uitloggen'])) {
	//Destroy huidige sessie
	session_destroy();
	header("Location: index.php");
}
echo '1';
$gebruikercontroller = new gebruikercontroller();
$gebruiker = $gebruikercontroller->getGebruiker($_SESSION['gebruiker_emailadres']);

?>

<?php require_once "templates/header.php"; ?>

<!DOCTYPE html>
<html lang="en">
<body>
<div class="container">
<div class="row">
<div class="col-lg-10">
<div class="page-header">
<h2>Account</h2>
</div>

<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
<div class="form-group ">
<label>Emailadres:</label> <label><?php echo $gebruiker->emailadres ?></label>
</div>
<div class="form-group ">
<label>Voornaam:</label> <label><?php echo $gebruiker->voornaam?></label>
</div>
<div class="form-group ">
<label>Achternaam:</label> <label><?php echo $gebruiker->achternaam ?></label>
</div>
<div class="form-group ">
<label>telefoonnummer:</label> <label><?php echo $gebruiker->telefoonnummer ?></label>
</div>
<div class="form-group ">
<label>rollen:</label> <label><?php
foreach ($gebruiker->rollen as $rol) {
    echo $rol->omschrijving; 
    echo ",<br>";
}
?> </label>
</div>
<br>
<input type="submit" class="btn btn-primary" name="uitloggen" value="Uitloggen">
</form>
</div>
</div>     
</div>
</body>
</html>

<?php require_once "templates/footer.php"; ?>