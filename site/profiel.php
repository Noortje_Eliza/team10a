<?php
require_once "templates/header.php";
require_once "config/database.php";
require_once "domein/gebruikercontroller.php";
require_once "domein/gebruiker.php";
require_once "domein/rol.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Profielpagina</title>
</head>
<body>
<div class="container">
    <table class="table">
        <thead>
        <tr>
            <th scope="col">Naam</th>
            <th scope="col">Achternaam</th>
            <th scope="col">Email</th>
            <th scope="col">Rol</th>
            <th scope="col">Telefoon nr.</th>
            <th scope="col">Inzien</th>
            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
            </tr>
            </thead>
            <?php
            $arr = array(1, 2, 3, 4);
            foreach ($arr as &$value) { ?>
                <tbody>
                <tr>
                <td>Johan</td>
                <td>Schippers</td>
                <td>j.schippers@enercon.com</td>
                <td>06-41236123</td>
                <td>Ploegleider</td>
                <td scope="col"><a href="#"><button type="button" class="btn btn-success">Inzien</button></a></td>
            </tr>
            </tbody>
            <?php
        }
        ?>
    </table>
</div>
<!--    <div class="container-sm">-->
<!--        <div class="row">-->
<!--            <div class="page-header">-->
<!--                <h2>Medewerker Overzicht</h2>-->
<!--            </div>-->
<!--        </div>-->
<!--        <div class="row">-->
<!--            <div class="col">-->
<!--                <!-- Search bar hier?-->
<!--            </div>-->
<!--            <div class="col text-end">-->
<!--                <button onclick="window.location.href='./profiel_aanmaken.php';" class="btn btn-primary">Profiel aanmaken</button>-->
<!--            </div>-->
<!--        </div>-->
<!--        <div class="row">-->
<!--            <div class="col">-->
<!--                <p><strong>Emailadres</strong></p>-->
<!--            </div>-->
<!--            <div class="col">-->
<!--                <p><strong>Achternaam</strong></p>-->
<!--            </div>-->
<!--            <div class="col">-->
<!--                <p><strong>Voornaam</strong></p>-->
<!--            </div>-->
<!--            <div class="col">-->
<!--                <p><strong>Telefoonnumer</strong></p>-->
<!--            </div>-->
<!--            <div class="col">-->
<!--                <p><strong>Rol(len)</strong></p>-->
<!--            </div>-->
<!---->
<!--        </div>-->
<!--        <div class="row">-->
<!--            --><?php
//            $gebruikerquery = "SELECT * FROM gebruiker";
//            $conn = databasecontroller::getInstance()->conn;
//            $resultaat = $conn->query($gebruikerquery);
//
//            $gebruikercontroller = new gebruikercontroller();
//            $gebruiker = $gebruikercontroller->getGebruiker('gebruiker_emailadres', 'gebruiker-emailadres','achternaam','telefoonnummer');
//
//            if ($resultaat->num_rows > 0) {
//                // output data of each row
//                while($row = $resultaat->fetch_assoc())
//                {?>
<!--                    <div class="row">-->
<!--                        <div class="col">-->
<!--                            --><?php //echo ""; ?>
<!--                        </div>-->
<!--                        <div class="col">-->
<!--                            --><?php //echo ""; ?>
<!--                        </div>-->
<!--                        <div class="col">-->
<!--                            --><?php //echo ""; ?>
<!--                        </div>-->
<!--                        <div class="col">-->
<!--                            --><?php //echo ""; ?>
<!--                        </div>-->
<!--                        <div class="col">-->
<!--                            --><?php //echo ""; ?>
<!--                        </div>-->
<!--                    </div>-->
<!--                --><?php
//                }
//            } else {
//                echo "0 results";
//            }
//            $conn->close();
//            ?>
<!--            <div class="row">-->
<!--                <div class="col">-->
<!--                    --><?php //echo "admin@enercon.com"; ?>
<!--                </div>-->
<!--                <div class="col">-->
<!--                    --><?php //echo "admin"; ?>
<!--                </div>-->
<!--                <div class="col">-->
<!--                    --><?php //echo "istrator"; ?>
<!--                </div>-->
<!--                <div class="col">-->
<!--                    --><?php //echo "12341234"; ?>
<!--                </div>-->
<!--                <div class="col">-->
<!--                    --><?php //echo "AD"; ?>
<!--                </div>-->
<!--            </div>-->
<!--            <div class="row">-->
<!--                <div class="col">-->
<!--                    --><?php //echo "janpietje@gmail.com"; ?>
<!--                </div>-->
<!--                <div class="col">-->
<!--                    --><?php //echo "jan"; ?>
<!--                </div>-->
<!--                <div class="col">-->
<!--                    --><?php //echo "pietje"; ?>
<!--                </div>-->
<!--                <div class="col">-->
<!--                    --><?php //echo "12331233"; ?>
<!--                </div>-->
<!--                <div class="col">-->
<!--                    --><?php //echo "IV"; ?>
<!--                </div>-->
<!--            </div>-->
<!--            <div class="row">-->
<!--                <div class="col">-->
<!--                    --><?php //echo "klaasjan@hotmail.com"; ?>
<!--                </div>-->
<!--                <div class="col">-->
<!--                    --><?php //echo "klaas"; ?>
<!--                </div>-->
<!--                <div class="col">-->
<!--                    --><?php //echo "jan"; ?>
<!--                </div>-->
<!--                <div class="col">-->
<!--                    --><?php //echo "12231223"; ?>
<!--                </div>-->
<!--                <div class="col">-->
<!--                    --><?php //echo "WV"; ?>
<!--                </div>-->
<!--            </div>-->
<!--            <div class="row">-->
<!--                <div class="col">-->
<!--                    --><?php //echo "hansjoker@gmail.com"; ?>
<!--                </div>-->
<!--                <div class="col">-->
<!--                    --><?php //echo "hans"; ?>
<!--                </div>-->
<!--                <div class="col">-->
<!--                    --><?php //echo "joker"; ?>
<!--                </div>-->
<!--                <div class="col">-->
<!--                    --><?php //echo "12351235"; ?>
<!--                </div>-->
<!--                <div class="col">-->
<!--                    --><?php //echo "PL"; ?>
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->

</body>
<?php
    require_once "templates/footer.php";
?>