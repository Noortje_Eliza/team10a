<?php 

require_once "config/database.php"; 
require_once "domein/gebruiker.php"; 
require_once "domein/rol.php"; 

class gebruikercontroller{

 public function login($emailadres, $wachtwoord){
	$conn = databasecontroller::getInstance()->conn;
	 // escape op sql injection
	$emailadres = mysqli_real_escape_string($conn, $emailadres);
	$wachwoord = mysqli_real_escape_string($conn, $wachtwoord);
	// haal gebruiker op en gebruik verify functie om met secure hash password te verifieren
	$result = mysqli_query($conn, "SELECT * FROM gebruiker WHERE emailadres = '" . $emailadres. "'");
	if ($row = mysqli_fetch_array($result)) {
		if(password_verify($wachtwoord, $row['wachtwoord'])) {
			// Succes, set sessie
			$_SESSION['gebruiker_achternaam'] = $row['achternaam'];
			$_SESSION['gebruiker_voornaam'] = $row['voornaam'];
			$_SESSION['gebruiker_emailadres'] = $row['emailadres'];
			return true;
		}	
	}
	return false;
}

 public function gebruikeraanmaken($emailadres, $wachtwoord, $voornaam, $achternaam, $telefoonnummer){
	$conn = databasecontroller::getInstance()->conn;
	$emailadres = mysqli_real_escape_string($conn, $emailadres);
	$wachwoord = mysqli_real_escape_string($conn, $wachtwoord);
	$voornaam = mysqli_real_escape_string($conn, $voornaam);
	$achternaam = mysqli_real_escape_string($conn, $achternaam);
	$telefoonnummer = mysqli_real_escape_string($conn, $telefoonnummer);
	//	TODO: Kijk of er niet reeds een gebruiker met dezelfde email bestaat, set error message

	$sql = "INSERT INTO gebruiker (voornaam, achternaam, telefoonnummer, emailadres, wachtwoord) VALUES('" . $voornaam. "', '" . $achternaam. "', '" . $telefoonnummer. "', '" . $emailadres. "', '" . password_hash($wachtwoord, PASSWORD_DEFAULT). "')";
	if (mysqli_query($conn, $sql) === TRUE) {
		return true;
	}
	return false;
}

public function getGebruiker($emailadres){
		$conn = databasecontroller::getInstance()->conn;
		 // haal gebruiker op 
		$resultgebruiker = mysqli_query($conn, "select * from gebruiker where emailadres = '" . $emailadres. "'");
		if ($row = mysqli_fetch_array($resultgebruiker)) {
			// gebruiker opgehaald, 
			$gebruiker_id = $row['gebruiker_id'];
			// nu rollen ophalen
			$resultrollen = mysqli_query($conn, "select code, omschrijving from gebruikerrolregel LEFT JOIN rol on gebruikerrolregel.rolid = rol.rolid where gebruikerid = " . $gebruiker_id );
			$rollen = array();
			while ($rowrol = mysqli_fetch_row($resultrollen)) {
				$rollen[] = new rol($rowrol[0], $rowrol[1]);
			}
			$gebruiker = new gebruiker($row['emailadres'], $row['voornaam'],$row['achternaam'],$row['telefoonnummer'], $rollen);
			mysqli_free_result($resultgebruiker);
			mysqli_free_result($resultrollen);
		
			return $gebruiker;
		}
		return false;
}
}
?>