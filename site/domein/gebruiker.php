<?php
class gebruiker{
  public $voornaam;
  public $achternaam;
  public $emailadres;
  public $telefoonnummer;
  public $rollen;
  function __construct($emailadres, $voornaam, $achternaam, $telefoonnummer,  $rollen){
	$this->emailadres = $emailadres;
	$this->voornaam = $voornaam;
	$this->achternaam = $achternaam;
	$this->telefoonnummer = $telefoonnummer;
	$this->rollen = $rollen;
 }
}