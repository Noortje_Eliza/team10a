<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.bundle.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <link rel="stylesheet" href="../css/style.css">
    <title>Enercon</title>
</head>
<body>
<div>
    <div class="container">
        <div class="row">
            <div class="col">
                <nav class="navbar navbar">
                    <a class="navbar-brand" href="landing.php">
                        <img src="images/enercon.png" width="200" height="auto" alt="" loading="lazy">
                    </a>
                    <ul class="nav">
                        <li class="nav-item">
                            <a class="nav-link" href="about.php">Over ons</a>
                        </li>
                        <li class="nav-item">
                            <div>
                                <?php
                                if(!isset($_SESSION))
                                {
                                    session_start();
                                }
                                if(isset($_SESSION['gebruiker_emailadres']) == ""){
                                    echo "<a  class='nav-link' href='inloggen.php'>Inloggen</a>";
                                } else {
                                    echo "<a class='nav-link' href='account.php'> Welkom " . $_SESSION['gebruiker_voornaam'] .' ' .  $_SESSION['gebruiker_achternaam']. "</a>";
                                }
                                ?>
                            </div>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</div>
<br> <br>