<?php
require_once("templates/header.php");
?>
    <div class="jumbotron jumbotron-fluid">
        <!-- Start container -->
        <div class="container">
            <h1 class="display-4">Archiefpagina</h1>
            <p class="lead">Hieronder alle schakelbrieven die zijn afgewerkt.</p>
        </div>
    </div>
    <div class="container">
    <table class="table">
        <thead>
        <tr>
            <th scope="col">Schakelbriefnummer</th>
            <th scope="col">Windpark</th>
            <th scope="col">Datum Werkzaamheden</th>
            <th scope="col">Status</th>
            <th scope="col">Inzien</th>
            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
        </tr>
        </thead>
        <?php
        $arr = array(1, 2, 3, 4);
        foreach ($arr as &$value) { ?>
            <tbody>
            <tr>
                <th>21683218</th>
                <td>Otto</td>
                <td>4-1-2021 21:00</td>
                <td>Afgerond</td>
                <td scope="col"><a href="#"><button type="button" class="btn btn-success">Inzien</button></a></td>
            </tr>
            </tbody>
            <?php
        }
        ?>
    </table>
    </div>
    </div>
    <!-- Einde container -->
<?php
require_once("templates/footer.php");
?>