<!DOCTYPE html>
<html lang="en">
<?php require_once "templates/header.php"; ?>

<?php
require_once "domein/gebruikercontroller.php";
// start sessie indien nodig
if(!isset($_SESSION)) 
{ 
	session_start(); 
} 

if(isset($_SESSION['gebruiker_emailadres'])!="") {
	//Indien reeds ingelogd dan moet er eerst uitgelogd worden
	header("Location: account.php");
}

if (isset($_POST['login'])) {
// haal post parameters op
$emailadres = $_POST['emailadres'];
$wachtwoord = $_POST['wachtwoord'];
// check input
if(!filter_var($emailadres,FILTER_VALIDATE_EMAIL)) {
$emailadres_error = "Geef een valide emailadres op.";
}
if(strlen($wachtwoord) < 1) {
$wachtwoord_error = "Geef een wachtwoord op.";
} 
$gebruikercontroller = new GebruikerController();
if($gebruikercontroller->login($emailadres,$wachtwoord)){
	// Redirect naar index.php na het inloggen
		header("Location: landing.php");
}
else{
	$error_message = "Onbekende combinatie emailadres en wachtwoord";
}
}
?>
<!DOCTYPE html>
<html lang="en">
<body>
<div class="container">
    <div class="wrapper">
        <div class="page-header">
        </div>
        <span class="text-danger"><?php if (isset($error_message)) echo $error_message; ?></span>
        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post" class="form-signin">
            <h2 class="form-signin-heading">Inloggen bij Enercon</h2>
            <hr class="colorgraph"><br>
            <div class="form-group ">
                <label>Emailadres</label>
                <input type="email" name="emailadres" class="form-control" value="" maxlength="100" required="">
                <span class="text-danger"><?php if (isset($email_error)) echo $emailadres_error; ?></span>
            </div>
            <div class="form-group">
                <label>Wachtwoord</label>
                <input type="password" name="wachtwoord" class="form-control" value="" maxlength="30" required="">
                <span class="text-danger"><?php if (isset($password_error)) echo $wachtwoord_error; ?></span>
            </div>
            <br>
            <input type="submit" class="btn btn-primary" name="login" value="Inloggen">
            <br><br>
            Nog geen account? <a href="account_aanmaken.php" class="mt-3">Klik dan hier</a>
        </form>
    </div>
</div>
</body>
</html>

<?php require_once "templates/footer.php"; ?>