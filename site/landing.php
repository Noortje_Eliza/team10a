
<?php require_once "templates/header.php"; ?>

<!--Boxen aanmaken, die verdeeld zijn onder elkaar over de hele pagina. col-sm-4-->
<div class="section section-landingpage-header">
    <div class="container container-landingpage-header">
        <div class="row row-landingpage-header">
            <div class="col-sm-12 col-header">
            </div>
        </div>
    </div>
</div>
<div class="section section-landingpage-images">
    <div class="container container-landingpage-img">
        <div class="row row-landingpage-img">
            <!--            Blok 1-->
            <div class="col-sm-4 landing-block">
                <a href="#">Ploegleider</i></a>
            </div>
            <div class="col-sm-4 landing-block">
                <a href="#">Installatie Verantwoordelijke</i></a>
            </div>
            <div class="col-sm-4 landing-block">
                <a href="#"></i>Werkverantwoordelijke</a>
            </div>
            <!--            Blok 2-->
            <div class="col-sm-4 landing-block">
                <a href="schakelbrief.php"></i>Schakelbrieven</a>
            </div>
            <div class="col-sm-4 landing-block">
                <a href="archief.php"></i>Archief</a>
            </div>
            <div class="col-sm-4 landing-block">
                <a href="#">Overzicht gebruikers</i></a>
            </div>
        </div>
        </div>
    </div>
</div>
<?php require_once "templates/footer.php"; ?>