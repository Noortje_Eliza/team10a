<!DOCTYPE html>
<html lang="en">
<?php require_once "templates\header.php"; ?>

<?php
require_once "domein/gebruikercontroller.php";

if (isset($_POST['account_aanmaken'])) {
	// haal post parameters op
	$emailadres = $_POST['emailadres'];
	$wachtwoord = $_POST['wachtwoord'];
	$achternaam = $_POST['achternaam'];
	$voornaam =$_POST['voornaam'];
	$telefoonnummer =$_POST['telefoonnummer'];

	$valid = true;
	// check input
	if(!filter_var($emailadres,FILTER_VALIDATE_EMAIL)) {
		$emailadres_error = "Vul een geldig email adres in.";
		$valid = false;
	}
	// todo meer validaties
	if(strlen($wachtwoord) < 6) {

		$wachtwoord_error = "Wachtwoord moet minimaal ";
		$valid = false;
	} 

	if(strlen($voornaam) < 1) {
		$voornaam_error = "Naam moet ingevuld zijn";
		$valid = false;
	} 

	if(strlen($achternaam) < 1) {
		$achternaam_error = "Naam moet ingevuld zijn";
		$valid = false;
	} 

	if(strlen($telefoonnummer) < 1) {
		$telefoonnummer_error = "Naam moet ingevuld zijn";
		$valid = false;
	} 

	if($valid){ 
		$gebruikercontroller = new GebruikerController();
		if($gebruikercontroller->gebruikeraanmaken($emailadres,$wachtwoord, $voornaam, $achternaam, $telefoonnummer) == true){
		// Account is aangemaakt, laat gebruiker inloggen
			header("Location: inloggen.php");
		} else {
			$error_message = "Error: gebruiker kan niet worden aangemaakt, mogelijk bestaat er al een account voor dit emailadres";
		}
	}
}

?>
<!DOCTYPE html>
<html lang="en">
<body>
<div class="container">
<div class="row">
<div class="col-lg-10">
<div class="page-header">
<h2>Account aanmaken</h2>
</div>
<span class="text-danger"><?php if (isset($error_message)) echo $error_message; ?></span>
<!-- form post actie op self -->
<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
<div class="form-group ">
<label>Emailadres</label>
<input type="email" name="emailadres" class="form-control" value="" maxlength="100" required="">
<span class="text-danger"><?php if (isset($email_error)) echo $email_error; ?></span>
</div>
<div class="form-group">
<label>Voornaam</label>
<input type="text" name="voornaam" class="form-control" value="" maxlength="100" required="">
<span class="text-danger"><?php if (isset($voornaam_error)) echo $voornaam_error; ?></span>
</div>  
<div class="form-group">
<label>Achternaam</label>
<input type="text" name="achternaam" class="form-control" value="" maxlength="100" required="">
<span class="text-danger"><?php if (isset($achternaam_error)) echo $achternaam_error; ?></span>
</div>  
<div class="form-group">
<label>Telefoonnummer</label>
<input type="text" name="telefoonnummer" class="form-control" value="" maxlength="100" required="">
<span class="text-danger"><?php if (isset($telefoonnummer_error)) echo $achternaam_error; ?></span>
</div> 
<div class="form-group">
<label>Wachtwoord</label>
<input type="password" name="wachtwoord" class="form-control" value="" maxlength="30" required="">
<span class="text-danger"><?php if (isset($wachtwoord_error)) echo $wachtwoord_error; ?></span>
</div>
<br>
<input type="submit" class="btn btn-primary" name="account_aanmaken" value="Account aanmaken">
<br><br>
Inloggen? <a href="inloggen.php" class="mt-3">Klik dan hier</a>
</form>
</div>
</div>     
</div>
</body>
</html>


<?php require_once "templates/footer.php"; ?>