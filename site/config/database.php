<?php

class databasecontroller {
	// Singleton instance
	private static $instance = null;
    static $servername='localhost';
	static $username='root';
	static $password='';
	static $dbname = "enercon";

	public $conn;
		
	// Private constructor voor DB verbinding, zodat er meer 1 instantie is per sessie, want willen we 1 db verbinding
	private function __construct(){
		$this->conn = mysqli_connect(self::$servername,self::$username,self::$password,self::$dbname);
		// Geef error asl we niet kunnen verbinden
		if(!$this->conn){
          die('Kan niet verbinden met MySql Server:' .mysql_error());
        }
	}

	// methode die singleton instantie aanmaakt
	public static function getInstance()
	{
    if (self::$instance == null)
    {
      self::$instance = new databasecontroller();
    }
 
    return self::$instance;
  }
}
		
?>